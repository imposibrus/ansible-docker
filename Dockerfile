FROM docker:stable as docker
FROM williamyeh/ansible:alpine3
COPY --from=docker /usr/local/bin/docker /usr/local/bin/docker
